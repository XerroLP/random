package pw.xerro.frozenbundle;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.configuration.file.FileConfiguration;

public class JoinListener implements Listener {
	
	@SuppressWarnings("unused")
	public static List<Player> staff = new ArrayList<Player>();
	private final FrozenBundle plugin;
	private List<String> plugins = new ArrayList<String>();
	private String pluginList = "";
	
	public JoinListener(FrozenBundle instance) {
	      plugin = instance;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player player = e.getPlayer();
        if(player.hasPermission("frozenbundle.staff")){
                staff.add(player);
                player.sendMessage("Looks like you're staff!");
        }
    }
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		final Player player = e.getPlayer();
        if(player.hasPermission("frozenbundle.staff")){
                staff.remove(player);
        }
    }
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if (e.getMessage().toLowerCase().startsWith("/plugins")
				|| e.getMessage().toLowerCase().startsWith("/pl")
				|| e.getMessage().toLowerCase().startsWith("/?")){
			e.setCancelled(true);
			plugins = plugin.getConfig().getStringList("plugins");
			for(String a : plugins) {
				pluginList = pluginList+a+"&a, ";
			}
			if(pluginList.length() > 0) {
				pluginList = pluginList.substring(0, pluginList.length()-2);
			}
			else {
				pluginList = "Something went wrong here..";
			}
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fPlugins (-1): &a"+pluginList));
		}
	}
}
