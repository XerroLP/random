package pw.xerro.frozenbundle;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.configuration.file.FileConfiguration;

public final class FrozenBundle extends JavaPlugin implements Listener{
//	private List<String> plugins = new ArrayList<String>();
//	private String pluginList = "";
	private String staffList = "";
	private JoinListener playerListener;
	
	@Override
    public void onEnable() {
		this.saveDefaultConfig();
        // TODO Insert logic to be performed when the plugin is enabled
		getLogger().info("Thank you for choosing FrozenMC for your Bukkit needs!");
		playerListener = new JoinListener(this);
		PluginManager manager = getServer().getPluginManager();
        manager.registerEvents(playerListener, this);
    }
 
    @Override
    public void onDisable() {
        // TODO Insert logic to be performed when the plugin is disabled
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
    	if(!(sender instanceof Player)) {
    		sender.sendMessage("Only players may execute this command.");
    	}
    	else {
    		if (cmd.getName().equalsIgnoreCase("frozenmc") 
    				|| cmd.getName().equalsIgnoreCase("fmc") 
    				|| cmd.getName().equalsIgnoreCase("frozenbundle")) {
    			if(args[0].equalsIgnoreCase("reload")) {
    				this.reloadConfig();
    			}
    			else {
    				sender.sendMessage(ChatColor.GREEN + "Thank you for choosing "
    						+ ChatColor.AQUA + ChatColor.BOLD + "FrozenBundle!");
    				sender.sendMessage(ChatColor.GREEN + "Version: "
    						+ ChatColor.WHITE + "1.0");
    				sender.sendMessage(ChatColor.GREEN + "By: " + ChatColor.WHITE
    						+ "XerroLP");
    				sender.sendMessage(ChatColor.GREEN + "Follow me at "
    						+ ChatColor.AQUA + "@xerrolp");
    				sender.sendMessage(ChatColor.GREEN + "Check out my website: "
    						+ ChatColor.AQUA + "http://xerro.pw");
    				sender.sendMessage(ChatColor.GREEN
    						+ "See more of my plugins on my website above!");
    			}
    			return true;
    		}
    		if (cmd.getName().equalsIgnoreCase("list")
					|| cmd.getName().equalsIgnoreCase("elist")
					|| cmd.getName().equalsIgnoreCase("who")
					|| cmd.getName().equalsIgnoreCase("online")) {
				try {
					int playerCount = this.getServer().getOnlinePlayers().length;
					for (Player p : JoinListener.staff) {
						staffList = staffList + p.getName() + ChatColor.AQUA
								+ ", ";
					}
					staffList = staffList.substring(0, staffList.length());
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig()
							.getString("list.header")));
					sender.sendMessage(ChatColor.DARK_GREEN + ""
							+ ChatColor.BOLD + "There are currently "
							+ ChatColor.GREEN + ChatColor.BOLD + playerCount
							+ ChatColor.DARK_GREEN + ChatColor.BOLD
							+ " online.");
					sender.sendMessage(ChatColor.DARK_AQUA + ""
							+ ChatColor.BOLD + "Staff Online: " + staffList);
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', this.getConfig()
							.getString("list.footer")));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				return true;
			}
//    		if (cmd.getName().equalsIgnoreCase("plugins")
//    				|| cmd.getName().equalsIgnoreCase("pl")
//    				|| cmd.getName().equalsIgnoreCase("?")) {
//				plugins = this.getConfig().getStringList("list.plugins");
//				for(String a : plugins) {
//					pluginList = pluginList+a+", ";
//				}
//				if(pluginList.length() > 0) {
//					pluginList = pluginList.substring(0, pluginList.length()-2);
//				}
//				else {
//					pluginList = "Something went wrong here..";
//				}
//				sender.sendMessage("&fPlugins (-1): &a"+pluginList);
//				return true;
//			}
    	} //If this has happened the function will return true. 
            // If this hasn't happened the value of false will be returned.
    	return false; 
    }
}
